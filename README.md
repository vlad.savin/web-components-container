# Web Components Container

1. Add a web component as a submodule using 

```
$ git submodule add {WEB_COMPONENT_CLONE_URL} {OUTPUT_DIR}
```

The output directory is optional (by default, it's the child repository name).
In order to save the changes, we need to `commit` and `push` them upstream.

2. When there's a change in the submodule repository, in order to fetch those changes, we need to
```
$ git submodule update --remote
```
and then `commit` and `push` the changes, so that the link between the container project and the web component project persists, pointing to the latest commit on the submodule.

3. In order to clone the project, we need to use the recurse-submodules flag on the git clone command:
```
$ git clone --recurse-submodules {WEB_CONTAINER_CLONE_URL}
```

4. In order to work on both the web-component-catalog and web-components-container from the same editor, you must do some extra steps.

By default, with what's been described above, you can't update the submodule's repository, even if you make changes on the files in web-component-catalog and push from the container project.

That happens because the submodule is in a "HEAD detached state". If we want to work on the submodule, we must checkout the submodule's branch that we want to work on
```
$ cd web-components-catalog && git checkout master
```
Then, from the root directory, run
```
$ git submodule update --remote --merge
```
in order to make sure the submodule is up to date.

After you make changes to the code in web-component-catalog and `commit` both the submodule and container changes, you can run the push command from the root directory of the container, like this:
```
$ git push --recurse-submodules=on-demand
```

    Note:
    Working on a project with submodules is pretty much the same as a normal
    project, in terms of adding code, committing changes and pushing them to
    the main repository. The important operations that can be used on a normal
    development flow are presented above.